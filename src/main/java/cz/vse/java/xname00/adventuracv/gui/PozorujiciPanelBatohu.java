package cz.vse.java.xname00.adventuracv.gui;

import cz.vse.java.xname00.adventuracv.logika.Batoh;
import cz.vse.java.xname00.adventuracv.logika.Hra;
import cz.vse.java.xname00.adventuracv.logika.IHra;
import cz.vse.java.xname00.adventuracv.observer.Observer;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.VBox;

import java.util.Set;

public class PozorujiciPanelBatohu implements Observer {

    IHra hra = Hra.getSingleton();
    Batoh batoh = hra.getBatoh();
    VBox vBox = new VBox();
    FlowPane flowPane = new FlowPane();
    Label label = new Label("Věci v batohu:");

    public PozorujiciPanelBatohu() {
        batoh.addObserver(this);

        vBox.getChildren().addAll(label, flowPane);

        update();
    }

    public VBox getVBox() {
        return vBox;
    }

    @Override
    public void update() {
        flowPane.getChildren().clear();
        Set<String> nazvyVeci = batoh.getMnozinaVeci();
        for (String nazev :nazvyVeci) {
            Image image = new Image(PozorujiciPanelBatohu.class.getResourceAsStream(
                    "/zdroje/" + nazev + ".jpeg"
            ), 150.0, 150.0, false, false);
            ImageView imageView = new ImageView(image);
            flowPane.getChildren().add(imageView);
        }
    }
}
