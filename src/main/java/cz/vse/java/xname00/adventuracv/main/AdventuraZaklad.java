package cz.vse.java.xname00.adventuracv.main;

import cz.vse.java.xname00.adventuracv.gui.PozorujiciPanelBatohu;
import cz.vse.java.xname00.adventuracv.gui.PozorujiciPanelVychodu;
import cz.vse.java.xname00.adventuracv.gui.PozorujiciPlanekHry;
import cz.vse.java.xname00.adventuracv.logika.Hra;
import cz.vse.java.xname00.adventuracv.logika.IHra;
import javafx.animation.AnimationTimer;
import javafx.animation.RotateTransition;
import javafx.animation.TranslateTransition;
import javafx.application.Application;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.LongProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import javafx.util.Duration;

public class AdventuraZaklad extends Application {

    private final IHra hra = Hra.getSingleton();

    public static void main(String[] args) {
        launch(args);
    }

    private ImageView vlkView = new ImageView(new Image(AdventuraZaklad.class.getResourceAsStream(
            "/zdroje/vlk.png"
    ), 150, 150, false, false));

    private ImageView viewKarkulky = new ImageView(new Image(AdventuraZaklad.class.getResourceAsStream("" +
            "/zdroje/karkulka.png"), 100, 100, false, false));

    @Override
    public void start(Stage primaryStage) {
        BorderPane platno = new BorderPane();

        TextArea konzole = pripravKonzoli();
        HBox spodniBox = pripravSpodniBox(konzole);

        PozorujiciPlanekHry pozorujiciPlanekHry = new PozorujiciPlanekHry(viewKarkulky);

        platno.setLeft(new PozorujiciPanelBatohu().getVBox());
        platno.setRight(new PozorujiciPanelVychodu().getListView());
        platno.setBottom(spodniBox);
        platno.setCenter(konzole);
        AnchorPane planekHry = pozorujiciPlanekHry.getPlanekHry();
        platno.setTop(planekHry);

        planekHry.getChildren().addAll(vlkView);
        AnchorPane.setLeftAnchor(vlkView, 0.0);
        AnchorPane.setTopAnchor(vlkView, 0.0);

        MenuBar menuBar = pripravMenu();
        VBox menuAHraVBox = new VBox();
        menuAHraVBox.getChildren().addAll(menuBar, platno);

        Scene scene = new Scene(menuAHraVBox);
        scene.getStylesheets().addAll("/zdroje/stylesheet.css");
        animujVlka();
        animujKarkulku(scene);

        primaryStage.setScene(scene);
        primaryStage.show();
    }

    private void animujKarkulku(Scene scene) {
        double karkulcinaKonstanta = 300.0; // pixels per unit of this computer's clock.

        LongProperty lastUpdateTime = new SimpleLongProperty(0);
        DoubleProperty karkulkyPohybY = new SimpleDoubleProperty(0.0);
        DoubleProperty karkulkyPohybX = new SimpleDoubleProperty(0.0);

        AnimationTimer karkulkaAnimation = new AnimationTimer() {
            @Override
            public void handle(long timestamp) {
                if (lastUpdateTime.get() > 0) {
                    double elapsedSeconds = (timestamp - lastUpdateTime.get()) /
                            1_000_000_000.0;

                    // karkulkyPohybX.get() je příliš vysoké číslo, resp. je to počet pixelů za jednotku něčeho.
                    // to něco je vnitřní frekvence počítače.
                    // proto to v předchozím příkazu dělíme jedním GIGA Hertzem (bez měrných jednotek) a vypadá to aspoň nějak.
                    double zmenaY = elapsedSeconds * karkulkyPohybY.get();
                    double souradniceY = viewKarkulky.getTranslateY();
                    viewKarkulky.setTranslateY(souradniceY + zmenaY);

                    double deltaX = elapsedSeconds * karkulkyPohybX.get();
                    double souradniceX = viewKarkulky.getTranslateX();
                    viewKarkulky.setTranslateX(souradniceX + deltaX);
                }
                lastUpdateTime.set(timestamp);
            }
        };
        karkulkaAnimation.start();

        scene.setOnKeyPressed(event -> {
            KeyCode code = event.getCode();
            if (code == KeyCode.S) {
                karkulkyPohybY.set(karkulcinaKonstanta);
            }
            if (code == KeyCode.W) {
                karkulkyPohybY.set(-karkulcinaKonstanta);
            }
            if (code == KeyCode.A) {
                karkulkyPohybX.set(-karkulcinaKonstanta);
            }
            if (code == KeyCode.D) {
                karkulkyPohybX.set(karkulcinaKonstanta);
            }
        });

        scene.setOnKeyReleased(event -> {
            karkulkyPohybY.set(0.0);
            karkulkyPohybX.set(0.0);
        });

    }

    private void animujVlka() {
        TranslateTransition transition = new TranslateTransition();
        transition.setNode(vlkView);

        transition.setByX(-300.0);
        transition.setByY(300.0);

        transition.setDuration(Duration.millis(2000));

        transition.setAutoReverse(true);

        transition.setCycleCount(-1);

        transition.play();



        RotateTransition rotate = new RotateTransition();

        rotate.setNode(vlkView);

        rotate.setByAngle(360.0);

        rotate.setCycleCount(-1);

        rotate.setDuration(Duration.ONE);

        rotate.play();

    }

    private HBox pripravSpodniBox(TextArea konzole) {
        Label popisek = new Label("Zadej příkaz:\t");
        popisek.setFont(Font.font("Arial", FontWeight.BLACK, 14.0));
        TextField uzivatelskyVstup = new TextField();
        HBox spodniBox = new HBox();
        spodniBox.setAlignment(Pos.CENTER);
        spodniBox.getChildren().addAll(popisek, uzivatelskyVstup);

        uzivatelskyVstup.setOnAction(event -> {
            String vstup = uzivatelskyVstup.getText();
            String odpovedHry = hra.zpracujPrikaz(vstup);
            StringBuilder odpovedHrySNovymiRady = new StringBuilder().append('\n').append(odpovedHry).append('\n');
            // konzole.appendText(String.format("\n%1$s\n", odpovedHry));
            konzole.appendText(odpovedHrySNovymiRady.toString());
            uzivatelskyVstup.clear();
        });

        uzivatelskyVstup.requestFocus();
        return spodniBox;
    }

    private TextArea pripravKonzoli() {
        TextArea konzole = new TextArea();
        String uvitani = hra.vratUvitani() + "\n";
        konzole.setText(uvitani);
        konzole.setEditable(false);
        return konzole;
    }

    private MenuBar pripravMenu() {
        MenuBar menuBar = new MenuBar();
        Menu souborMenu = new Menu("Soubor");
        Menu napovedaMenu = new Menu("Nápověda");

        ImageView novaHraIkonka = new ImageView(new Image(AdventuraZaklad.class.getResourceAsStream("/zdroje/new.gif")));

        MenuItem novaHraMenuItem = new MenuItem("Nová Hra", novaHraIkonka);
        novaHraMenuItem.setOnAction(event -> {
            start(new Stage());
        });

        MenuItem konecMenuItem = new MenuItem("Konec");
        konecMenuItem.setOnAction(event -> System.exit(0));

        MenuItem oAplikaciMenuItem = new MenuItem("O Aplikaci");
        MenuItem napovedaMenuItem = new MenuItem("Nápověda");

        novaHraMenuItem.setAccelerator(KeyCombination.keyCombination("Ctrl+N"));

        // oddělovač
        SeparatorMenuItem separatorMenuItem = new SeparatorMenuItem();

        souborMenu.getItems().addAll(novaHraMenuItem, separatorMenuItem, konecMenuItem);
        napovedaMenu.getItems().addAll(oAplikaciMenuItem, napovedaMenuItem);

        oAplikaciMenuItem.setOnAction(event -> {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
//	... set title, content,...
            alert.setTitle("Java FX Adventura");
            alert.setHeaderText("Header Text - Java FX Adventura");
            alert.setContentText("Verze ZS 2021");
            alert.showAndWait();
        });

        napovedaMenuItem.setOnAction(event -> {
            Stage stage = new Stage();
            stage.setTitle("Napoveda k aplikaci");
            WebView webView = new WebView();
            webView.getEngine().load(AdventuraZaklad.class.getResource
                    ("/zdroje/napoveda.html").toExternalForm());
            stage.setScene(new Scene(webView, 600, 600));
            stage.show();
        });

        menuBar.getMenus().addAll(souborMenu, napovedaMenu);
        return menuBar;
    }
}
