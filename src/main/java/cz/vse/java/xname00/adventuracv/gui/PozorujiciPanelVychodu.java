package cz.vse.java.xname00.adventuracv.gui;

import cz.vse.java.xname00.adventuracv.logika.HerniPlan;
import cz.vse.java.xname00.adventuracv.logika.Hra;
import cz.vse.java.xname00.adventuracv.logika.IHra;
import cz.vse.java.xname00.adventuracv.logika.Prostor;
import cz.vse.java.xname00.adventuracv.observer.Observer;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.ListView;

public class PozorujiciPanelVychodu implements Observer {

    IHra hra = Hra.getSingleton();
    HerniPlan herniPlan = hra.getHerniPlan();

    // Publisher - Vydavatel - SubjectOfChange
    ObservableList<String> observableList = FXCollections.observableArrayList();
    // Subscriber - Odběratel - Observer
    ListView<String> listView = new ListView<>(observableList);

    public PozorujiciPanelVychodu() {
        herniPlan.addObserver(this);

        update();
    }

    public ListView<String> getListView() {
        return listView;
    }

    @Override
    public void update() {
        observableList.clear();
        for (Prostor prostor : herniPlan.getAktualniProstor().getVychody()) {
            observableList.add(prostor.getNazev());
        }
    }
}
