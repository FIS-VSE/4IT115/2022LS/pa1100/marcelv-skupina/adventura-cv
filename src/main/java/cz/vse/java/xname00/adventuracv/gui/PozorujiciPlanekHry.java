package cz.vse.java.xname00.adventuracv.gui;

import cz.vse.java.xname00.adventuracv.logika.HerniPlan;
import cz.vse.java.xname00.adventuracv.logika.Hra;
import cz.vse.java.xname00.adventuracv.logika.IHra;
import cz.vse.java.xname00.adventuracv.logika.Prostor;
import cz.vse.java.xname00.adventuracv.main.AdventuraZaklad;
import cz.vse.java.xname00.adventuracv.observer.Observer;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;

public class PozorujiciPlanekHry implements Observer {

    private final AnchorPane planekHry = new AnchorPane();
    private final IHra hra = Hra.getSingleton();
    private final HerniPlan herniPlan = hra.getHerniPlan();
    private final ImageView karkulka;


    public PozorujiciPlanekHry(ImageView viewKarkulky) {
        this.karkulka = viewKarkulky;

        herniPlan.addObserver(this);

        Image planekHryImage = new Image(
                AdventuraZaklad.class.getResourceAsStream("/zdroje/herniPlan.png"),
                400.0, 250.0, false, false
        );
        ImageView planekHryView = new ImageView(planekHryImage);


        planekHry.getChildren().addAll(planekHryView, karkulka);

        update();
    }

    public AnchorPane getPlanekHry() {
        return planekHry;
    }

    @Override
    public void update() {
        Prostor aktualniProstor = herniPlan.getAktualniProstor();

        AnchorPane.setLeftAnchor(karkulka, aktualniProstor.getX());
        AnchorPane.setTopAnchor(karkulka, aktualniProstor.getY());

        System.out.println("Prostor byl změněn na: " + aktualniProstor);

    }
}
